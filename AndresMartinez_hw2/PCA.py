import numpy as np
import matplotlib.pyplot as plt

dat = np.genfromtxt("siliconwaferthickness.csv",skip_header=1, dtype='float', delimiter=",")

fil = np.size(dat[:,0])
col = np.size(dat[0,:])
prom = np.zeros(col)


for i in range(col):
    dat[:,i]=(dat[:,i]-np.mean(dat[:,i]))/np.std(dat[:,i])
    prom[i] = np.mean(dat[:,i])

for i in range(col):
    plt.plot(dat[:,i], label="G"+str(i+1))
plt.xlabel("# de dato")
plt.ylabel("Grosor")
plt.legend(bbox_to_anchor=(1.05, 1), loc=1, borderaxespad=0., fontsize=10)
plt.savefig("ExploracionDatos.pdf")
plt.close()

B = np.zeros([col,col])
for i in range(col):
    for j in range(i,col):
        B[i,j] = np.sum(((dat[:,i]-prom[i])*(dat[:,j]-prom[j]))/(fil-1))
        B[j,i] = B[i,j]

AVal, AVect = np.linalg.eig(B)
LVect = []
for i in range(col):
    L=[]
    for j in range(np.size(AVect[:,i])):
        L.append(AVect[j,i])
    LVect.append(L)

link = sorted(zip(AVal,LVect), reverse=True)
AVal = [ elemento[0] for elemento in link]
AVect = np.array([ elemento[1] for elemento in link])
#AVect = AVect.T
print "Valores: \n",AVal
print "Vectores: (por filas, donde la primera fila corresponde al primer autovalor) \n",AVect

n = AVect.dot(dat.T)
plt.scatter(n[0,:], n[1,:], alpha=0.2, color="red")
plt.xlabel("PC1")
plt.ylabel("PC2")
plt.xlim( (-11, 11) )
plt.ylim( (-11, 11) )
plt.savefig("PCAdatos.pdf")
plt.close()

for i in range(np.size(AVect[:,0])):
    plt.scatter(AVect[0,i],AVect[1,i])
    if i%2!=0 or i==0:
        plt.annotate(i+1, (AVect[0,i]+0.03,AVect[1,i]))
    else:
        plt.annotate(i+1, (AVect[0,i]-0.07,AVect[1,i]))
plt.xlabel("PC1")
plt.ylabel("PC2")
plt.xlim( (-1, 1) )
plt.ylim( (-1, 1) )
plt.savefig("PCAvariables.pdf")
plt.close()

print "Las variables correlacionadas son: 1,2,3,4,5 y las 7,8,9. La variable 6 se encuentra alejada del resto de variables por lo que se considera independiente. "

