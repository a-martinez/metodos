import numpy as np
import matplotlib.pyplot as plt

def gauss(A):
    col = np.size(A[0,:])
    fil = np.size(A[:,0])
    if(col-1!=fil):
        print "La matriz no tiene la forma esperada"
        return None
    b = True
    while b:
        for i in range(fil):
            b = False
            if A[i,i]==0.:
                b = True
                o = fil-1
                for h in range(col-2):
                    if A[i,h]!=0:
                        t = np.copy(A[i,:])
                        A[i,:] = np.copy(A[h,:])
                        A[h,:] = t
    for i in range(fil):
        A[i,:] = A[i,:]/A[i,i]
        if i<(fil-1):
            for j in range(i+1):
                A[i+1,:] = A[i+1,:]-(A[j,:]*A[i+1,j])
    sol = np.zeros(fil)
    for i in range(fil-1,-1,-1):
        k = []
        for j in range(fil-1,i,-1):
            k.append(A[i,j]*sol[j])
        np.put(sol,i, A[i,col-1]-np.sum(np.array(k)))
    return sol


A = np.array([[4.,2.,1.,45.948],[25.,5.,1.,119.985],[81.,9.,1.,231.497]])
sol = gauss(A)
print "a1 = "+str(sol[0])+", a2 = "+str(sol[1])+", a3 = "+str(sol[2])
def fun(t):
    return (sol[0]*(t**2))+(sol[1]*t)+sol[2]
print "Para t = 7, v = ",fun(7)

plt.scatter(2.,45.948, label="(2 , 45.948)",c='g',s=80)
plt.scatter(5.,119.985, label="(5 , 119.985)",c='r',s=80)
plt.scatter(9.,231.497, label="(9 , 231.497)",c='y',s=80)
lin = np.linspace(1,12,96)
plt.plot(lin,fun(lin),label="v(t)")
plt.xlabel("t(s)")
plt.ylabel("v(m/s)")
plt.legend(bbox_to_anchor=(1.05, 1), loc=5, fontsize=8, borderaxespad=0.)
plt.savefig("VelocidadCohete.pdf")
plt.close()
