import numpy as np
import matplotlib.pyplot as plt

#ODE en la forma y'=f(x,y)
h=0.1
xmin = 0
xmax = 6

def f(x,y):
    return -y

def Euler(xi,yi):
    n = int((xmax - xmin)/h)
    x = np.zeros(n)
    y = np.zeros(n)
    #Condiciones iniciales
    x[0]=xi
    y[0]=yi
    for i in range(1,np.size(x)):
        x[i] = x[i-1] + h
        y[i] = y[i-1] + (h*f(x[i-1],y[i-1]))
    return x,y

a,b = Euler(0,1)
plt.plot(a,b)
plt.show()



