import numpy as np
import matplotlib.pyplot as plt

evoluciones = 700
puntos = 1000
xmin = -2.
xmax = 2.
dx = (xmax-xmin)/puntos

linea1 = np.linspace(-2,2,1000)
dx = linea1[1]-linea1[0]
dt = 0.001

sol = np.exp(-100*((linea1+(dt*evoluciones))**2))
linea1 = np.exp(-100*(linea1**2))

xlimmin = 0.
xlimmax = 0.
linea1[0] = xlimmin
linea1[puntos-1] = xlimmax

c = 1.
const = c*dt/dx

for i in range(evoluciones):
    linea1[1:-1] = linea1[1:-1]+(const*(linea1[1:-1]-linea1[:-2]))

plt.plot(np.linspace(-2,2,1000),linea1)
plt.plot(np.linspace(-2,2,1000),sol)
plt.show()
