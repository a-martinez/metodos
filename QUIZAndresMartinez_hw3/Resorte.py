import numpy as np
import matplotlib.pyplot as plt


def fun(x):
    k=300.
    m=2.
    return (-k*x)/m

puntos = 1000000
x = np.empty(puntos)
v = np.empty(puntos)
x[0]=0.1
v[0]=0.
h=5./puntos
for i in range(1,puntos):
    v[i] = v[i-1] + (h*fun(x[i-1]))
    x[i]=x[i-1]+(h*v[i-1])

plt.plot(np.linspace(0,5,puntos),x)
plt.savefig("Resorte.pdf")
plt.close()

