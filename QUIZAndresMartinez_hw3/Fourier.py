import numpy as np
import matplotlib.pyplot as plt

dat = np.loadtxt("violin.dat")
datFtt = np.fft.fft(dat)
sd = 1./44100.
freq = np.fft.fftfreq(np.size(dat),sd)

plt.plot(freq,datFtt)
plt.savefig("Fourier.pdf")
plt.close()

for i in range(np.size(dat)):
    if (freq[i]>=1000 or freq[i]<=-1000):
        datFtt[i]=0

plt.plot(freq,datFtt)
plt.savefig("filtro_fourier.pdf")
plt.close()

plt.plot(np.linspace(0,(np.size(dat))*sd,np.size(dat)),np.real(np.fft.ifft(datFtt)))
plt.savefig("violin_filtrado.pdf")
plt.close()

