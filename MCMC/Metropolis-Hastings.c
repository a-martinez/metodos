#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <time.h>

double rand01(){
    return (double)rand()/(double)RAND_MAX;
}

double randMax(double n){
    return ((double)rand()/(double)RAND_MAX)*n;
}

double randNormal(double ancho, double centro){
    double d1 = rand01();
    double d2 = rand01();
    
    if(d1==0){
        return centro;
    }
    else if(d1>0.5){
        return (ancho*log(d2))+centro;
    }
    else{
        return (-ancho*log(d2))+centro;
    }
}

double funcion(double x){
    //return exp(-(x*x))/(((x-3)*(x-3)) + 0.0001);
    if(x>10 || x<-10){
        return 0;
    }
    return (-x*x)+4;
}

double * Metropolis(double sigma, int lanzamientos,double * puntos){
    double act = 2.0*(rand01()-0.5);
    double temp;
    double actf;
    double tempf;
    double rand2;
    for(int i=0;i<lanzamientos;i++){
        temp = randNormal(sigma,act);
        actf = funcion(act);
        tempf = funcion(temp);
        rand2 = rand01();
        if(actf<tempf||(tempf/actf)>rand2){
            puntos[i] = temp;
            act = temp;
        }else{
            puntos[i] = act;
        }
    }
    return puntos;
}


int main(){
    srand(time(NULL));
    
    double puntos[1000000];
    Metropolis(0.1, 1000000,puntos);
    
    FILE *in = fopen("Metro.txt","w");
    for(int i=0; i<1000000; i++){
        fprintf(in,"%f\n",puntos[i]);
    }
    fclose(in);
}


