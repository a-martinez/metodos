import numpy as np
import matplotlib.pyplot as plt

def gauss(sigma, central, x):
    return np.exp((-(np.power((x-central),2)))/(2*np.power(sigma,2)))/(np.sqrt(2*np.pi*sigma*sigma))


d=np.random.rand(1000)
d1=np.random.rand(1000)

y1 = np.log(d)

for i in range(1000):
    if(d1[i]>0.5):
        y1[i]=-y1[i]

y1 = y1+10

plt.hist(np.sort(y1),bins=50)
plt.show()






