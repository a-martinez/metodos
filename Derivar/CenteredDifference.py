import numpy as np

def func(a):
    return a

def derivada(a,b,x):
    l = np.linspace(a,b,x)
    h = l[1]-l[0]
    m = func(l)
    n = (m[2:]-m[:np.size(m)-2])
    return n/h

print np.size(derivada(1.,100.,100000.))
