import numpy as np
import matplotlib.pyplot as plt

Datos1 = np.loadtxt("DatosMarzo.txt")
Datos2 = np.loadtxt("GRF_vs_EQ.txt")

plt.scatter(Datos2[:,1],Datos2[:,0],c='k',s=20, label="Todos los meses")
plt.scatter(Datos1[:,1],Datos1[:,0],c='g',s=200, label="Marzo")
plt.xlabel("Largest Earthquake")
plt.ylabel("Glacier && Rock Fall")
plt.legend(bbox_to_anchor=(0., 1.02, 1., .102), loc=3,
           ncol=2, mode="expand", borderaxespad=0.)

plt.savefig("PlotTolima.pdf")
