mkdir Tolima
cd Tolima
mv ../DatosTolima.dat DatosTolima.dat
mv ../PlotsTolima.py PlotsTolima.py



tail -n +2 DatosTolima.dat|sed 's/2006//'|sed 's/2007//'|sed 's/2008//'|sed 's/2009//'|sed 's/2010//'|sed 's/2011//'|sed 's/2012//' > Datos.txt
awk '{if($1=="March")print $4 " " $6}' Datos.txt > DatosMarzo.txt
awk '{print $4 " " $6}' Datos.txt > GRF_vs_EQ.txt

rm DatosTolima.dat
rm Datos.txt
python PlotsTolima.py
