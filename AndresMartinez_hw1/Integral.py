import numpy as np
import matplotlib.pyplot as plt


def f(x,y):
    return (x+(np.cos(y)*x))**3

def montecarlo(a,b,n):
    x = np.random.random(n)*a
    y = np.random.random(n)*b
    result = f(x,y).mean()
    return result*a*b

print "Por montecarlo la integral es: ", montecarlo(np.pi,1.,10000)

def int_riemman(a,b,x):
    l = np.linspace(0,a,x)
    k = np.linspace(0,b,x)
    h1 =a/x
    h2 =b/x
    p = 0
    for i in l:
        n = 0
        for j in k:
            n = n+f(i,j)*h2
        p = p+(n*h1)
    return p

print "Por sumas de Riemman es: ",int_riemman(np.pi,1.,1000)
