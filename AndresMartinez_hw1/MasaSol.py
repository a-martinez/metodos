import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D


t = np.loadtxt("EarthOrbit.dat") 
m = np.loadtxt("MarsOrbit.dat")

X=t[:,1]*(1.4996e11)
Y=t[:,2]*(1.4996e11)
Z=t[:,3]*(1.4996e11)
Xm=m[:,1]*(1.4996e11)
Ym=m[:,2]*(1.4996e11)
Zm=m[:,3]*(1.4996e11)


fig = plt.figure()
ax = plt.axes(projection='3d')

ax.plot(X, Y, Z, color='k')
ax.plot(Xm, Ym, Zm, color='b')
ax.set_xlabel('eje X')
ax.set_ylabel('eje Y')
ax.set_zlabel('eje Z')
ax.text(X[1],Y[1],Z[1],"Tierra",color='r')
ax.text(Xm[200],Ym[200],Zm[200],"Marte",color='r')
fig.savefig("Orbita.pdf")

def derivar2(A, h):
	return (A[2:]+A[:-2]-(2*A[1:-1]))/(h**2)

h = t[2,0]-t[1,0]
axT = derivar2(X,h)
ayT = derivar2(Y,h)
azT	=derivar2(Z,h)
h = m[2,0]-m[1,0]
axM = derivar2(Xm,h)
ayM = derivar2(Ym,h)
azM = derivar2(Zm,h)

G=6.674e-11

aR = np.sqrt(((axT)**2)+((ayT)**2)+((azT)**2))
r = np.sqrt(((X[1:-1])**2)+((Y[1:-1])**2)+((Z[1:-1])**2))
print "La masa del sol obtenida a partir de las posiciones de la tierra es ",np.mean(((r**2)*aR)/G), "kg"

aR = np.sqrt(((axM)**2)+((ayM)**2)+((azM)**2))
r = np.sqrt(((Xm[1:-1])**2)+((Ym[1:-1])**2)+((Zm[1:-1])**2))
print "La masa del sol obtenida a partir de las posiciones de la marte es ", np.mean(((r**2)*aR)/G), "kg"


