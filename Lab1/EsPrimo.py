from IsPrime import esPrimo

print "Ingrese 0 para salir"
var = 1
while(var!=0):
	var = raw_input("Ingrese un numero entero: ")
	try:
		var = int(var)
		print "El valor ingresado es", var
	except:
		print "El valor ingresado no es valido!"
		continue
	if(esPrimo(var)):
		print "El numero "+str(var)+" es primo"
	else:
		print "El numero "+str(var)+" no es primo"
