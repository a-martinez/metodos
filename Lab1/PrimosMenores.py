import numpy as np
from PrimesLessThan import pMenores

print "Ingrese 0 para salir"
var = 1
while(var!=0):
	var = raw_input("Ingrese un numero entero: ")
	try:
		var = int(var)
		print "El valor ingresado es", var
	except:
		print "El valor ingresado no es valido!"
		continue
	print "Los primos menores a", var, "son", pMenores(var)
