from IsPrime import esPrimo
import numpy as np
from PrimesLessThan import pMenores



def goldbach(n):
    for i in pMenores(n):
        p=1
        s=0
        while(s<n):
            s=i+(2*(p**2))
            if(s==n):
                # print n,"=", i,"+","2 x",p,"^2" #mostrar los numeros necesarios para que se cumpla la conjetura
                return True
            p=p+1
    return False

n=3 # debe ser impar y mayor a 1, sabiendo el resultado puede fijarse en 5501 para mayor velocidad
h = 0
print "Calculando numeros que violan la conjetura menores a 10 mil... (aprox. 18 sec)"
while(n<10000):
    if(not esPrimo(n)):
        if(not goldbach(n)):
            if(h==0):
                print "Primer numero: ", n
                h = 1
            else:
                print "Segundo numero: ", n, "gato"
                break
    n=n+2


