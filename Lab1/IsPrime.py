def esPrimo(number):
    if(number<=1.0 or (number%2.0==0.0 and number!=2.0)):
        # print 0
        return False
    n = int(number/2.)
    if(n%2==0.0):
        n=n+1
    while(n>1):
        if(number%n==0.0):
            # print 0
            return False
        n=n-2
    # print 1
    return True
