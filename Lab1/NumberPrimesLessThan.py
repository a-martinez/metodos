from PrimesLessThan import pMenores
import numpy as np
import matplotlib.pyplot as plt
from scipy import special

def NumberPrimesLessThan(x):
    b = []
    for i in x:
        b.append(len(pMenores(i)))
    # print b
    return b

a = np.linspace(1,1000,1000)
b = NumberPrimesLessThan(a)

plt.plot(a,b)
plt.show()
