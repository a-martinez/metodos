from IsPrime import esPrimo
import numpy as np
l=[2.0]
def pMenores(numero):
    if(numero<3):
        # print []
        return []
    d=len(l)-1
    if (l[d]>=numero):
        while(l[d]>numero):
            d=d-1
        # print list(l[0:d+1])
        return list(l[0:d+1])
    else:
        a = np.linspace(l[d]+1,numero-1,numero-l[d]-1)
        if(a.size==0):
        # print l
            return l
        n=0
        if(a[0]%2==0.0):
            n=1
        for i in a[n::2]:
            if(esPrimo(i)):
                l.append(i)
    # print l
    return l
