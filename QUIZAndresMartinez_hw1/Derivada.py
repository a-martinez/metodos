import numpy as np
import matplotlib.pyplot as plt

datos = np.loadtxt("Balon.dat")

t = datos[:,0]
x = datos[:,1]
h = t[1]-t[0]

derivada = (x[2:]-x[0:-2])/(2*h)

plt.plot(t[1:-1],derivada)
plt.xlabel("tiempo en (s)")
plt.ylabel("velocidad en (m/s)")
plt.savefig("Derivada.pdf")
