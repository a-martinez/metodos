import numpy as np
import matplotlib.pyplot as plt
a = 3
p = np.linspace(0,a,1000)
v = 1
y = []

for n in p:
    f = 0
    for i in range(0,50000,1):
        f = f+(4*v/(((2*i)+1)*np.pi))*np.sin((((2*i)+1)*np.pi)*(n/a))
    y.append(f)
y = np.array(y)
plt.scatter(p,y)
plt.show()
