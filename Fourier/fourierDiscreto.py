import numpy as np
import matplotlib.pylab as plt

n = 500. # number of point in the whole interval
f = 200.0 #  frecuencia en Hz
dt = 1 / (f * 32 ) #32 muestras por unidad de frecuencia
t = np.linspace( 0, (n-1)*dt, n)
p = np.linspace(0,n-1,n)

y = np.sin(2 * np.pi * f * t)-np.cos(4 * np.pi * f * t)

#plt.plot(t,y)
#plt.show()

h = []
for i in p:
    h.append(np.sum(y*(np.exp(-p*1j*2*np.pi*(i/n)))))
h = np.array(h)
print h/np.fft.fft(y)
plt.plot(p/(n*dt),h)
plt.xlim(0,1000)
plt.show()
print dt


