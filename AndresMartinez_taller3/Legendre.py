import numpy as np

def funcion(a):
    return np.absolute(((5*(a**3))-(3*a))/2)**2


def monteCarlo(a,b,x):
    l = funcion((np.random.random(x)*(b-a))-1)
    return np.mean(l)*(b-a)

def error(teorico, real):
    return np.absolute((teorico-real)*100/real)

h = monteCarlo(-1.,1.,1000000)
b = error(2./7.,h)



print "Valor numerico:",h, "Valor terorico:",2./7.,"Error:",b
