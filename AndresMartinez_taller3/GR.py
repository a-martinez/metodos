import numpy as np
import matplotlib.pyplot as plt


h = np.loadtxt("magnitudes.txt")

plt.hist(h,bins=5)
plt.show()



plt.plot(np.log(np.linspace(1,np.size(h),np.size(h))),np.cumsum(h))
plt.show()
