import numpy as np
import matplotlib.pyplot as plt

def fur(X,n):
    funcion = []
    b = np.linspace(-n,n,2*n+1)
    for x in X:
        suma = 0
        for t in b:
            if not (t==0):
                suma+=(((-1.)**(np.abs(t)))*np.exp(1j*t*x)*1j*((np.pi*np.pi/t)-(6./(t**3))))
        funcion.append(suma)
    return np.array(funcion)

X=np.linspace(-np.pi,np.pi,1000)

X=np.concatenate((X, X+2*np.pi))
h = fur(X,2)
h1 = np.concatenate((h, h+2*np.pi))
print np.shape(h1)
print np.shape(X)
plt.plot(X,h1,label="n=2")
'''
plt.plot(X,fur(X,5),label="n=5")
plt.plot(X,fur(X,20),label="n=20")
plt.plot(X,fur(X,200),label="n=200")
plt.plot(X,X**3,label="x^3",c="y")
'''
plt.legend(loc=2)
plt.title("Fourier numerico x^3")
plt.savefig("Grafica.pdf")
plt.close()
