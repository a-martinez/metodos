import numpy as np
import matplotlib.pyplot as plt

dat = np.genfromtxt("adveccion.txt", delimiter=";", dtype="float")

plt.subplot(2,1,1)
plt.plot(dat[:,0],dat[:,1],label="t=0s")
plt.title("Adveccion")
plt.legend()

plt.subplot(2,1,2)
plt.title("Adveccion")
plt.plot(dat[:,0],dat[:,2],label="t=0.7s")
plt.legend()

plt.savefig("Grafica.png")
plt.show()
