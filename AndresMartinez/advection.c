#include <stdio.h>
#include <math.h>
#include <stdlib.h>

int main(){
    int evoluciones = 700;
    int puntos = 1000;
    double xmin = -2.;
    double xmax = 2.;
    double dx = (xmax-xmin)/puntos;
    
    double linea1[puntos];
    double linea2[puntos];
    double tiempo[puntos];
    double inicial[puntos];
    double dt = 0.001;
    
    double a;
    
    for(int i=0; i<puntos; i++){
        a=xmin+(i*dx);
        tiempo[i]=a;
        if(fabs(tiempo[i])<=0.5){
            linea1[i]=-pow(a,2)+0.25;
        }
        else{
            linea1[i]=0;
        }
        inicial[i]=linea1[i];
    }
    
    linea1[0] = 0.;
    linea1[puntos-1] = 0.;
    linea2[0] = 0.;
    linea2[puntos-1] = 0.;
    
    double c = 1.;
    double con = c*dt/dx;
    
    for(int k=0; k<evoluciones; k++){
        for(int i=1; i<puntos-1; i++){
            linea2[i]=linea1[i]+(con*linea1[i-1]);
        }
        for(int i=1; i<puntos-1; i++){
            linea1[i]=linea2[i];
        }
    }
    FILE *in = fopen("adveccion.txt","w");
    for(int i=0; i<puntos; i++){
        fprintf(in,"%f;%f;%f\n",tiempo[i],inicial[i],linea1[i]);
    }
    fclose(in);
}

