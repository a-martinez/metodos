import numpy as np
import matplotlib.pyplot as plt

#x=t
#y_1=x
#y_2=vx
#y_3=y
#y_4=vy

q = -1.76*(10.0**11)
B = 5.0*(10.0**-5)
lam =10.0**-11
e = 9.0*(10.0**-12)

min_x = 0.0
max_x = 0.00002
n_points = 2000
h=(max_x)/n_points
x = np.zeros(n_points)
y_1 = np.zeros(n_points)
y_2 = np.zeros(n_points)
y_3 = np.zeros(n_points)
y_4 = np.zeros(n_points)

x[0]   = min_x
y_1[0] = 2.0
y_2[0] = 0.0
y_3[0] = 2.0
y_4[0] = -3000.0



def func_prime_1(x, y_1, y_2, y_3, y_4):
    return y_2

def func_prime_2(x, y_1, y_2, y_3, y_4):
    return (q*B*y_4)+((y_1*lam*q)/(2*np.pi*e*((y_1*y_1)+(y_3*y_3))))

def func_prime_3(x, y_1, y_2, y_3, y_4):
    return y_4

def func_prime_4(x, y_1, y_2, y_3, y_4):
    return (-q*B*y_2)+((y_3*lam*q)/(2*np.pi*e*((y_1*y_1)+(y_3*y_3))))



def RungeKuttaFourthOrderStep(x_old, y1_old, y2_old, y3_old, y4_old):
    
    k_1_prime1 = func_prime_1(x_old,y1_old, y2_old, y3_old, y4_old)
    k_1_prime2 = func_prime_2(x_old,y1_old, y2_old, y3_old, y4_old)
    k_1_prime3 = func_prime_3(x_old,y1_old, y2_old, y3_old, y4_old)
    k_1_prime4 = func_prime_4(x_old,y1_old, y2_old, y3_old, y4_old)
    
    #first step
    x1 = x_old+ (h/2.0)
    y1_1 = y1_old + (h/2.0) * k_1_prime1
    y2_1 = y2_old + (h/2.0) * k_1_prime2
    y3_1 = y3_old + (h/2.0) * k_1_prime3
    y4_1 = y4_old + (h/2.0) * k_1_prime4
    k_2_prime1 = func_prime_1(x1, y1_1, y2_1, y3_1, y4_1)
    k_2_prime2 = func_prime_2(x1, y1_1, y2_1, y3_1, y4_1)
    k_2_prime3 = func_prime_3(x1, y1_1, y2_1, y3_1, y4_1)
    k_2_prime4 = func_prime_4(x1, y1_1, y2_1, y3_1, y4_1)
    
    #second step
    x2 = x_old + (h/2.0)
    y1_2 = y1_old + (h/2.0) * k_2_prime1
    y2_2 = y2_old + (h/2.0) * k_2_prime2
    y3_2 = y3_old + (h/2.0) * k_2_prime3
    y4_2 = y4_old + (h/2.0) * k_2_prime4
    k_3_prime1 = func_prime_1(x2, y1_2, y2_2, y3_2, y4_2)
    k_3_prime2 = func_prime_2(x2, y1_2, y2_2, y3_2, y4_2)
    k_3_prime3 = func_prime_3(x2, y1_2, y2_2, y3_2, y4_2)
    k_3_prime4 = func_prime_4(x2, y1_2, y2_2, y3_2, y4_2)
    
    
    #third
    x3 = x_old + h
    y1_3 = y1_old + h * k_3_prime1
    y2_3 = y2_old + h * k_3_prime2
    y3_3 = y3_old + h * k_3_prime3
    y4_3 = y4_old + h * k_3_prime4
    k_4_prime1 = func_prime_1(x3, y1_3, y2_3, y3_3, y4_3)
    k_4_prime2 = func_prime_2(x3, y1_3, y2_3, y3_3, y4_3)
    k_4_prime3 = func_prime_3(x3, y1_3, y2_3, y3_3, y4_3)
    k_4_prime4 = func_prime_4(x3, y1_3, y2_3, y3_3, y4_3)
    
    #fourth step
    average_k_1 = (1.0/6.0)*(k_1_prime1 + 2.0*k_2_prime1 + 2.0*k_3_prime1 + k_4_prime1)
    average_k_2 = (1.0/6.0)*(k_1_prime2 + 2.0*k_2_prime2 + 2.0*k_3_prime2 + k_4_prime2)
    average_k_3 = (1.0/6.0)*(k_1_prime3 + 2.0*k_2_prime3 + 2.0*k_3_prime3 + k_4_prime3)
    average_k_4 = (1.0/6.0)*(k_1_prime4 + 2.0*k_2_prime4 + 2.0*k_3_prime4 + k_4_prime4)
    
    x_new = x_old + h
    y_1_new = y1_old + h * average_k_1
    y_2_new = y2_old + h * average_k_2
    y_3_new = y3_old + h * average_k_3
    y_4_new = y4_old + h * average_k_4
    return x_new, y_1_new, y_2_new, y_3_new, y_4_new

for i in range(1,n_points):
    x[i],y_1[i],y_2[i],y_3[i],y_4[i] = RungeKuttaFourthOrderStep(x[i-1], y_1[i-1], y_2[i-1], y_3[i-1], y_4[i-1] )

plt.plot(y_1,y_3)
plt.title("X vs Y")
#plt.savefig("grafica.pdf")
plt.show()


