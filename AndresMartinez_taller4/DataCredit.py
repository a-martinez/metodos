import numpy as np

DataCredit = np.genfromtxt("DataCredit.csv")

for i in range(np.size(DataCredit[0,:])):
    DataCredit[:,i]=(DataCredit[:,i]-np.mean(DataCredit[:,i]))/np.std(DataCredit[:,i])

covarianza = np.cov(DataCredit.T)

AutoValores, AutoVectores = np.linalg.eig(covarianza)

print AutoVectores
print AutoValores
print np.dot(DataCredit, AutoVectores) #pasar datos a la nueva base
