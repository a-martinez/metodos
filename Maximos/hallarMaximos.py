import numpy as np

text_file = open("Output1.txt", "w")
text_file.write("Datos intensidad \n \n")

u = np.loadtxt("0.dat")[:,1]
u[::-1].sort()
text_file.write("Angulo: 0, " + str(np.mean(u[:5])) + ", var: " + str(np.var(u[:5])) + "\n")
for i in range(10,110,10):
    n=str(i)+".dat"
    n1="m"+str(i)+".dat"
    if(i==100):
        h = np.loadtxt(n)
        u = h[:,1]
        u[::-1].sort()
        m = "Angulo: " + str(i) + ", positivo: " + str(np.mean(u[:5])) + ", var: " + str(np.var(u[:5])) + ", negativo: --" + "\n"
        text_file.write(m)
        continue
    h = np.loadtxt(n)
    h1 = np.loadtxt(n1)
    u = h[:,1]
    u[::-1].sort()
    u1 = h1[:,1]
    u1[::-1].sort()
    m = "Angulo: " + str(i) + ", positivo: " + str(np.mean(u[:5])) + ", var: " + str(np.var(u[:5])) + ", negativo: " + str(np.mean(u1[:5])) + ", var: " + str(np.var(u1[:5])) + "\n"
    text_file.write(m)
text_file.write("\n")
u = np.loadtxt("solo.dat")[:,1]
u[::-1].sort()
text_file.write("Sin polarizador: "+str(np.mean(u[:5])) + ", var: " + str(np.var(u[:5]))+"\n")
u = np.loadtxt("1en0.dat")[:,1]
u[::-1].sort()
text_file.write("Con un solo polarizador a 0 grados: "+str(np.mean(u[:5])) + ", var: " + str(np.var(u[:5]))+"\n")
u = np.loadtxt("1en90.dat")[:,1]
u[::-1].sort()
text_file.write("Con un solo polarizador a 90 grados: "+str(np.mean(u[:5])) + ", var: " + str(np.var(u[:5]))+"\n")


text_file.close()
