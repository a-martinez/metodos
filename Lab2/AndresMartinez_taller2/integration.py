import numpy as np
import matplotlib.pyplot as plt


# funcion a integrar
def f(x,y):
    return np.exp(-(x**2+y**2))

# Calcular la integral usando MonteCarlo
def montecarlo(a,b,n):
    x = np.random.random(n)*a
    y = np.random.random(n)*b
    f_eval = f(x,y)
    result = f_eval.mean()
    return result*a*b


print montecarlo(10,10,100000)

# Calcular la integral usando metodo de trapezoides
def trapezoid(a, b, n):
    y = np.linspace(0, a, n+1)
    h1 = a/n
    result1 = 0
    for j in range(n+1):
        x = np.linspace(0, b, n+1)
        h = (b)/n
        result = 0
        for i in range(n+1):
            if i == 0 or i == n:
                weight = h/2.
            else:
                weight = h
                result += f(x[i],y[j])*weight
        result1 += result*h1
    return result1

print trapezoid(10.,10.,1000)














