import numpy as np

def funcion(a):
    return a**2


def int_riemman(a,b,x):
    l = funcion(np.arange(a,b,(b-a)/x))
    l = l*((b-a)/x)
    return np.sum(l)


print int_riemman(1.,2.,1000000.)
