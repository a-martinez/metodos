import numpy as np

def funcion(a):
    return a**2

def int_simpson(a,b,x):
    n = (b-a)/x
    l = funcion(np.arange(a,b+n,n))*n
    l[2:np.size(a)-2:2] *= 2
    l[1::2] *= 4
    l /= 3.
    return np.sum(l)


print int_simpson(1.,2.,1000.)

