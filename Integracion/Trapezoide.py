import numpy as np

def funcion(a):
    return a**2


def int_trapezoide(a,b,x):
    n = (b-a)/x
    p = np.arange(a,b+n,n)
    print np.size(p)
    l = (funcion(p))*n
    np.put(l,[0,np.size(l)-1],[l[1]/2.,l[np.size(l)-1]])
    return np.sum(l)


print int_trapezoide(1.,2.,1000000.)
