import numpy as np
import matplotlib.pyplot as plt

dat = np.genfromtxt("Derivada.txt", delimiter=";", dtype="float")

plt.plot(dat[:,0],dat[:,1])
plt.savefig("derivada.png")
