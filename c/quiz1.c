#include <stdio.h>
#include <stdlib.h>

int cargarDatos(float al[101],float bl[101]){
    FILE *in = fopen("Piedra.dat","r");
    float a;
    float b;
    char c[100];
    int n;
    int k=0;
    n = fscanf(in, "%s %s %s %s\n",c,c,c,c);
    do{
        n = fscanf(in, "%f %f\n",&a,&b);
        if(n==EOF){
            break;
        }
        al[k]=a;
        bl[k]=b;
        /*
        printf("%f,%f\n",al[k],bl[k]);
         */
        k++;
    }while(1);
    fclose(in);
    return 0;
}

int main(){
    int Size = 101;
    float Tiempo[Size];
    float Pos[Size];
    float Deri[Size-2];
    cargarDatos(Tiempo,Pos);
    FILE *in = fopen("Derivada.txt","w");
    for(int i = 1; i<Size-1; i++){
        Deri[i-1] = (Pos[i+1]-Pos[i-1])/(Tiempo[i+1]-Tiempo[i-1]);
        fprintf(in,"%f;%f\n",Tiempo[i],Deri[i-1]);
    }
    fclose(in);
    
    return 0;
}

