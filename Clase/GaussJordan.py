import numpy as np


def gauss(A):
# va fila y despues columna
	col = np.size(A[0,:])
	fil = np.size(A[:,0])
	if(col-1!=fil):
		print "La matriz no tiene la forma esperada"
		return None
	print "Matriz ingresada:\n",A
	b = True
	while b:
		for i in range(fil):
			b = False
			if A[i,i]==0.:
				b = True
				o = fil-1
				for h in range(col-2):
					if A[i,h]!=0:
						t = np.copy(A[i,:])
						A[i,:] = np.copy(A[h,:])
						A[h,:] = t
	print "Matriz reordenada:\n",A
	for i in range(fil):
		A[i,:] = A[i,:]/A[i,i]
		if i<fil-1:
			for j in range(i+1):
				A[i+1,:] = A[i+1,:]-(A[j,:]*A[i+1,j])
	print "Matriz reducida:\n",A

	sol = np.zeros(fil)
	for i in range(fil-1,-1,-1):
		print i
		k = []
		for j in range(fil-1,i,-1):
			k.append(A[i,j]*sol[j])
		np.put(sol,i, A[i,col-1]-np.sum(np.array(k)))
		print A[i,col-1],np.sum(np.array(k))
	solucion = np.copy(sol[::-1])
	print "Solucion a las variables en orden:\n",solucion
	return solucion
		

A = np.array([[0.,2.,3.,6.,1.],[5.,0.,6.,2.,8.],[7.,6.,5.,5.,3.],[8.,4.,2.,0.,3.]])

gauss(A)
