import numpy as np
import matplotlib.pyplot as plt


A = np.genfromtxt("room-temperature.csv",  usecols = range( 1, 5), delimiter=",",  skip_header=1)

m = np.size(A[:,0])
n = np.size(A[0,:])
prom = np.array([np.mean(A[:,0]),np.mean(A[:,1]),np.mean(A[:,2]),np.mean(A[:,3])])


B = np.zeros([n,n])

for i in range(n):
    for j in range(n):
        B[i,j]= np.sum(((A[:,i]-prom[i])*(A[:,j]-prom[j]))/(m-1))
print B

print np.cov(A.T)

AutoValores, AutoVectores = np.linalg.eig(B)
H = np.dot(A, AutoVectores)
K = np.array([[1.,0.,0.,0.],[0.,1.,0.,0.],[0.,0.,1.,0.],[0.,0.,0.,1.]])
P = np.dot(K, AutoVectores)
print H
print AutoVectores
print AutoValores
plt.scatter(P[:,0],P[:,1])
plt.show()
