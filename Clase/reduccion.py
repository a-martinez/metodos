import numpy as np


def gauss(A):
    print A
    col = np.size(A[0,:])
    fil = np.size(A[:,0])
    if(col!=fil):
        print "La matriz no tiene la forma esperada"
        return None
    for i in range(fil):
        if(A[i,i]==0.):
            print "Existen 0 en la diagonal"
            return None
    for i in range(fil):
        A[i,:] = A[i,:]/A[i,i]
        if i<fil-1:
            for j in range(i+1):
                A[i+1,:] = A[i+1,:]-(A[j,:]*A[i+1,j])
    return A


A = np.array([[7.,2.,3.,6.],[5.,7.,6.,2.],[7.,6.,5.,5.],[8.,4.,2.,8.]])

print gauss(A)
