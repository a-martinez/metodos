import numpy as np
import matplotlib.pylab as plt
from scipy import misc


plt.subplots_adjust(hspace=0.2,wspace=0.2)
    
plt.subplot(2,2,1)
plt.title("Barcelona")
Barcelona = misc.imread('Barcelona.jpg', flatten=True)
plt.imshow(Barcelona, cmap=plt.cm.gray)
plt.gca().axes.get_yaxis().set_visible(False)
plt.gca().axes.get_xaxis().set_visible(False)

plt.subplot(2,2,2)
plt.title("Paris")
Paris = misc.imread('Paris.jpg', flatten=True)
plt.imshow(Paris, cmap=plt.cm.gray)
plt.gca().axes.get_yaxis().set_visible(False)
plt.gca().axes.get_xaxis().set_visible(False)

plt.subplot(2,2,3)
plt.title("Fractal")
frac = misc.imread('frac.jpeg', flatten=True)
plt.imshow(frac, cmap=plt.cm.gray)
plt.gca().get_yaxis().set_visible(False)
plt.gca().axes.get_xaxis().set_visible(False)

plt.subplot(2,2,4)
plt.title("Triangulos")
triangulos = misc.imread('triangulos.png', flatten=True)
plt.imshow(triangulos, cmap=plt.cm.gray)
plt.gca().axes.get_yaxis().set_visible(False)
plt.gca().axes.get_xaxis().set_visible(False)
    
plt.savefig("imagenes.pdf",dpi=500,bbox_inches='tight')
plt.close()


plt.subplots_adjust(hspace=0.2,wspace=0.2)

plt.subplot(2,2,1)
plt.title("Barcelona")
fftBarcelona = np.fft.fft2(Barcelona)
plt.imshow(np.log(abs(fftBarcelona)), cmap=plt.cm.gray)
plt.gca().axes.get_yaxis().set_visible(False)
plt.gca().axes.get_xaxis().set_visible(False)

plt.subplot(2,2,2)
plt.title("Paris")
fftParis = np.fft.fft2(Paris)
plt.imshow(np.log(abs(fftParis)), cmap=plt.cm.gray)
plt.gca().axes.get_yaxis().set_visible(False)
plt.gca().axes.get_xaxis().set_visible(False)

plt.subplot(2,2,3)
plt.title("Fractal")
fftFrac = np.fft.fft2(frac)
plt.imshow(np.log(abs(fftFrac)), cmap=plt.cm.gray)
plt.gca().get_yaxis().set_visible(False)
plt.gca().axes.get_xaxis().set_visible(False)

plt.subplot(2,2,4)
plt.title("Triangulos")
fftTriangulos = np.fft.fft2(triangulos)
plt.imshow(np.log(abs(fftTriangulos)), cmap=plt.cm.gray)
plt.gca().axes.get_yaxis().set_visible(False)
plt.gca().axes.get_xaxis().set_visible(False)

plt.savefig("transformadas.pdf",dpi=500,bbox_inches='tight')
plt.close()


plt.subplots_adjust(hspace=0.3,wspace=0.3)

plt.subplot(2,2,1)
plt.title("Barcelona")
plt.plot((fftBarcelona[np.size(fftBarcelona[:,0])/2,:]).real)

plt.subplot(2,2,2)
plt.title("Paris")
plt.plot((fftParis[np.size(fftParis[:,0])/2,:]).real)

plt.subplot(2,2,3)
plt.title("Fractal")
plt.plot((fftFrac[np.size(fftFrac[:,0])/2,:]).real)

plt.subplot(2,2,4)
plt.title("Triangulos")
plt.plot((fftTriangulos[np.size(fftTriangulos[:,0])/2,:]).real)

plt.savefig("cortes_transversales.pdf",dpi=500,bbox_inches='tight')
plt.close()



for i in range(6):
    fftBarcelona[:,i][1:] = fftBarcelona[:,i][1:]*1e-5
    fftBarcelona[:,-(i+1)][1:] = fftBarcelona[:,-(i+1)][1:]*1e-5
#fftBarcelona[0,:] = fftBarcelona[0,:]*1.05
#fftBarcelona[-1,:] = fftBarcelona[-1,:]*1.3


'''
#Filtro para borrar diagonal
alto=np.size(Barcelona[:,0])
ancho=np.size(Barcelona[0,:])
m=float(ancho)/alto
print m
p=0
j=0
for i in range(alto):
    p=p+m
    if(p>2):
        j=j+2
        p=p-2
    elif(p<2 and p>1):
        j=j+1
        p=p-1
    if(j>ancho-2):
        break;
#print i,j
    fftBarcelona[i,j]=0
    fftBarcelona[i,j-1]=0
    fftBarcelona[i,j+1]=0
    
    
plt.imshow(np.log(abs(fftBarcelona)), cmap=plt.cm.gray)
plt.gca().axes.get_yaxis().set_visible(False)
plt.gca().axes.get_xaxis().set_visible(False)
plt.show()
'''

plt.title("Barcelona sin horizontales")
plt.imshow(abs(np.fft.ifft2(fftBarcelona)), cmap=plt.cm.gray)
plt.gca().axes.get_yaxis().set_visible(False)
plt.gca().axes.get_xaxis().set_visible(False)

plt.savefig("sin_horizontales.pdf",dpi=500,bbox_inches='tight')
plt.close()



