import numpy as np
import matplotlib.pylab as plt

def f1(b,g,I,S):
    return -b*I*S

def f2(b,g,I,S):
   return (b*I*S)-(g*I)

def f3(b,g,I,S):
  return (g*I)

def Leap_frog(b,g,tmin,tmax,puntos,I0,S0):
    t=np.empty(puntos)
    S=np.empty(puntos)
    I=np.empty(puntos)
    R=np.empty(puntos)
    S[0]=S0
    I[0]=I0
    R[0]=0
    t[0]=tmin
    h=(tmax-tmin)/puntos
    
    S[1]=S[0]+(f1(b,g,I[0],S[0])*h)
    I[1]=I[0]+(f2(b,g,I[0],S[0])*h)
    R[1]=R[0]+(f3(b,g,I[0],S[0])*h)
    t[1]=t[0]+h
    for i in range(1,puntos-1):
        S[i+1]=S[i-1]+(2.*f1(b,g,I[i],S[i])*h)
        I[i+1]=I[i-1]+(2.*f2(b,g,I[i],S[i])*h)
        R[i+1]=R[i-1]+(2.*f3(b,g,I[i],S[i])*h)
        t[i+1]=t[i]+h
    return S,I,R,t


plt.subplots_adjust(hspace=0.5)

plt.subplot(2,1,1)
S,I,R,t = Leap_frog(0.0022, 0.45,0.,35.,1000,1.,770.)
plt.plot(t,S,label="Sana", c="y")
plt.plot(t,I,label="Infectada", c="r")
plt.plot(t,R,label="Recuperados", c="k")
plt.xlabel("Numero de dias")
plt.ylabel("# de personas")
plt.xlim((0,35))
plt.ylim((0,800))
plt.title("Caso 1")
a = "Imax: "+str(t[np.argmax(I)])+" dias"
plt.legend(title=a,loc=7,prop={'size': 12})
print "caso 1, tiempo I_max(dias): ",t[np.argmax(I)]

plt.subplot(2,1,2)
S1,I1,R1,t1 = Leap_frog(0.001, 0.2,0.,35.,1000,1.,770.)
plt.plot(t1,S1,label="Sana", c="y")
plt.plot(t1,I1,label="Infectada", c="r")
plt.plot(t1,R1,label="Recuperados", c="k")
plt.xlabel("Numero de dias")
plt.ylabel("# de personas")
plt.xlim((0,35))
plt.ylim((0,800))
plt.title("Caso 2")
a = "Imax: "+str(t1[np.argmax(I1)])+" dias"
plt.legend(title=a,loc=7,prop={'size': 12})
print "caso 2, tiempo I_max(dias): ",t1[np.argmax(I1)]

plt.savefig("SIR.pdf",dpi=500,bbox_inches='tight')
plt.close()
