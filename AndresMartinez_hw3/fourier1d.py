import numpy as np
#import matplotlib.pylab as plt

dat = np.genfromtxt("funcion.dat",skip_header=1, dtype='float')

#plt.plot(dat[:,0],dat[:,1])
#plt.show()

N_Puntos = np.size(dat[:,0])
t = dat[:,0]
F = dat[:,1]
p = np.linspace(0,N_Puntos-1,N_Puntos)

h = []
for i in p:
    h.append(np.sum(F*(np.exp(-p*1j*2*np.pi*(i/N_Puntos)))))
h = np.array(h)

SR = 1/t[1]
p[:(N_Puntos/2)]=p[:(N_Puntos/2)]
p[(N_Puntos/2):]=np.arange(-(N_Puntos/2),0,dtype=int)
p=((p*SR)/N_Puntos)

#plt.plot(p,h)
#plt.show()

frec = p[np.argmax(h[:N_Puntos/2])]

print "La frecuencia es: ", frec,"Hz, donde ", frec,"Hz es el resultado obtenido con el analisis de Fourier."
